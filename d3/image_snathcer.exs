matcher = ~r/\.(jpg|jpeg|png|gif|bmp)$/
# matched_files = Files.ls!() |> Enum.filter(fn x -> Regex.match?(matcher, x))
matched_files = Files.ls!() |> Enum.filter(&Regex.match?(matcher, &1))

num_mathced = Enum.count(matched_files)

msg_end =
  case num_mathced do
    1 -> "file"
    _ -> "files"
  end

IO.puts("Mathced #{num_mathced} #{msg_end}")

case File.mkdir("./images") do
  {:ok} -> IO.puts("./images directory successfully created")
  {:error, _} -> IO.puts("Could not create./images directory")
end

Enum.each(matched_files, fn filename ->
  case File.rename(filename, "./images/#{filename}") do
    {:ok} -> IO.puts("#{filename} successfully moved to images directory")
    {:error, _} -> IO.puts("Error moving #{filename} to images directory")
  end
end)
