defmodule Greeter do
  @author "Mohammed"
  def greet() do
    name = IO.gets("What is your name?\n")
    |> String.trim()
    if name == @author do
      "Wow, #{author} is my favorite name. I was programmed by someone named #{@author}"
    else
      "have a nice day #{name}"
    end
  end
end
