defmodule MinimalTodo do
  def start() do
    input =
      IO.gets("Would you like to create new .csv? (y/N)")
      |> String.trim()
      |> String.downcase()

    if input == "y" do
      create_initial_todo() |> get_command()
    else
      load_csv()
    end
  end

  def add_todo(data) do
    # get the name of todo
    name = data |> get_item_name()
    # get fields and values
    fields =
      data
      |> get_fields
      |> Enum.map(fn field -> field_from_user(field) end)

    # tuple into map
    # merge
    %{name => Enum.into(fields, %{})}
    |> Map.merge(data)
    |> get_command()
  end

  def create_header(headers) do
    case IO.gets("Add field: ") |> String.trim() do
      "" ->
        headers

      header ->
        create_header([header | headers])
    end
  end

  def create_headers() do
    IO.puts(
      "What data should each Todo have?\n" <>
        "Enter field names one by one and empty line if you're done.\n"
    )

    create_header([])
  end

  def create_initial_todo() do
    IO.puts(
      "What data should each Todo have?\n" <>
        "Enter field names one by one and an empty line when you're done.\n"
    )

    titles = create_headers()
    name = get_item_name(%{})
    fields = Enum.map(titles, &field_from_user(&1))
    IO.puts(~s/New todo "#{name}" added./)
    %{name => Enum.into(fields, %{})}
  end

  def delete_todo(data) do
    todo = IO.gets("Which Todo would you like to delete ?\n") |> String.trim()

    if Map.has_key?(data, todo) do
      IO.puts("ok.")
      new_todo = Map.drop(data, [todo])
      IO.puts(~s(#{todo} has been deleted.))
      get_command(new_todo)
    else
      IO.puts(~s(There is no Todo named #{todo}!))
      show_todo(data, false)
      delete_todo(data)
    end
  end

  def field_from_user(name) do
    field = IO.gets("#{name} : ") |> String.trim()
    {name, field}
  end

  def get_command(data) do
    prompt = """
    Type the first letter of the command you want to run

    R)ead Todos   A)dd Todo   D)elete Todo   S)ave .csv   L)oad a .csv   Q)uit
    """

    command =
      IO.gets(prompt)
      |> String.trim()
      |> String.downcase()

    case command do
      "a" -> add_todo(data)
      "r" -> show_todo(data)
      "d" -> delete_todo(data)
      "s" -> save_csv(data)
      "l" -> load_csv()
      "q" -> "Goodbye!"
      _ -> get_command(data)
    end
  end

  def get_item_name(data) do
    name = IO.gets("Enter the name of the new todo: ") |> String.trim()

    if Map.has_key?(data, name) do
      IO.puts("Todo with that name already exists.\n")
      get_item_name(data)
    else
      name
    end
  end

  def get_fields(data) do
    data[hd(Map.keys(data))] |> Map.keys()
  end

  def load_csv() do
    filename = IO.gets("Name of csv file to load : ") |> String.trim()

    read_csv(filename)
    |> parse
    |> get_command
  end

  def parse(body) do
    [header | lines] = body |> String.split(~r{(\r\n|\n|\r)})
    titles = tl(String.split(header, ","))
    parse_lines(lines, titles)
  end

  def parse_lines(lines, titles) do
    Enum.reduce(lines, %{}, fn line, built ->
      [name | fields] = String.split(line, ",")

      if Enum.count(fields) == Enum.count(titles) do
        line_data = Enum.zip(titles, fields) |> Enum.into(%{})
        Map.merge(built, %{name => line_data})
      else
        built
      end
    end)
  end

  def prepare_csv(data) do
    headers = ["Item" | get_fields(data)]
    data |> Map.keys() |> IO.puts()
    items = Map.keys(data)

    item_rows =
      Enum.map(items, fn item ->
        [item | Map.values(data[item])]
      end)

    rows = [headers | item_rows]
    row_strings = Enum.map(rows, &Enum.join(&1, ","))
    Enum.join(row_strings, "\n")
  end

  def read_csv(filename) do
    case File.read(filename) do
      {:ok, body} ->
        body

      {:error, reason} ->
        IO.puts("Could not read file.\n")
        IO.puts(~s(#{:file.format_error(reason)}))
        start()
    end
  end

  def save_csv(data) do
    filename = IO.gets("Name of .csv to save: ") |> String.trim()
    file_data = prepare_csv(data)

    case(File.write(filename, file_data)) do
      :ok ->
        IO.puts("CSV saved")
        get_command(data)

      {:error, reason} ->
        IO.puts(" Couldn't saved csv #{filename}")
        IO.puts(~s("#{:file.format_error(reason)}"" \n))
        get_command(data)
    end
  end

  def show_todo(data, next_command? \\ true) do
    todos = Map.keys(data)
    IO.puts("You have the following Todos: \n")
    Enum.each(todos, fn item -> IO.puts(item) end)
    IO.puts("\n")

    if next_command? do
      get_command(data)
    end
  end
end
