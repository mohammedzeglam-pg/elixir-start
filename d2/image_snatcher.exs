path =
  IO.gets("Insert working directory\n")
  |> String.trim()
  |> Path.expand()

# check if dir exisits
if File.dir?(path) do
  IO.puts("Directory found successfully")
else
  IO.puts("Directory not found")
end

dir = Path.expand(path <> "/images/")
# get files in directory
files =
  case File.ls(path) do
    {:ok, files} -> files
    {:error, reason} -> IO.inspect(reason)
  end

# filter images
images =
  files
  |> Enum.filter(fn x ->
    x
    |> String.match?(~r/(.png|.jpg|.jpeg|gif)/)
  end)

# create images folder
if !File.dir?(dir) do
  File.mkdir!(dir)
end

# copy images
Enum.each(images, fn img ->
  IO.inspect(File.cp!(path <> "/" <> img, dir <> "/" <> img))
end)
