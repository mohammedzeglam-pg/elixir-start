filename = IO.gets("File to count words from (h for help):\n") |> String.trim
if filename === "h" do
  """
  Usage: [filename] -[flags]
  Flags
  -l displays line count
  -c displays character count
  -w displays word count (default)

  Multiple flags may be used. Example usage to display line and character count:

  smallfile.txt -lc

  """ |> IO.puts
else
  parts = filename |> String.split("-")
  filename = parts |> List.first |> String.trim
  flags = case parts |> Enum.at(1) do
    nil -> ["w"]
    chars -> chars |> String.split("") |> Enum.filter(fn x -> x != "" end)
  end
    body = filename |> File.read!
    lines = body |> String.split(~r{(\r\n|\w|\r)})
    words = body |> String.split(~r{(\\n|[^\w'])+})
    chars = body |> String.split("") |> Enum.filter(fn x -> x != "" end)
    Enum.each(flags, fn flag ->
      case flag do 
        "l" -> "Lines: #{lines |> Enum.count}" |> IO.puts
        "w" -> "Words: #{words |> Enum.count}" |> IO.puts
        "c" -> "chars: #{chars |> Enum.count}" |> IO.puts
        _ -> nil
      end
    end)
end
